## Normative Model Methodology

# The Energy Model

The normative model is a simplified physics based energy model. The model is comprised of algebraic heat balance equations which are evaluated at monthly intervals. The model is considered normative because it uses normative assumptions for operating characteristics like schedules and occupancy. The simplified equations used to model the buildings physics that describe heating, cooling, ventilation, hot water, lighting and plug loads are based on the methods defined in the ISO 13790 (2006) standard. 

This process uses energy and asset data from a portfolio to create and calibrate a normative model for each building. These calibrated energy models can be used to determine the current level of energy efficiency as well as test scenarios with energy conservation measures to see how much energy could be saved in each building. The savings in each building can then be compared and aggregated across the portfolio of Marriotts.


### Model Inputs
* Location / Climate Zone
* Weather data including outdoor air temperature and solar radiation
* Building Type
* Building Area
* One year of total monthly energy data including Electricity, Natural Gas, Chilled Water and Steam

# Method 

### Reference Models
The first step in the process of developing a calibrated energy model is to use the building type and building area to find a matching reference model. The reference models I built for this project are based on US Department of Energy Commercial Reference Buildings and have different wall and window properties based on each climate zone. There are 16  Commercial Reference Building Types that represent about 70% of the commercial buildings in the US based on a report by the National Renewable Energy Laboratory. We could create more detailed custom models beyond the 16 reference types but would need detailed drawings including window and wall construction.

In analyzing a portfolio there must be a manual mapping of buildings to a space type.  Considerations for this are the uidling size and the building characteristics if you must choose between to buildings that are not perfect matches.


### Heating and Cooling Detection
Once a reference model is selected the script determines the heating fuel by evaluating the correlation of outdoor air temperature and average energy consumption per day for each fuel. For this analytical approach it is required you have all energy consumption data which is typically at least electricity and natural gas consumption but some also consume purchased chilled water and steam. So each building can be heated by electricity, natural gas, steam, or a combination of fuels as a hybrid. The figure of the hotel below is an example where the heating indicator is a strong correlation between natural gas consumption and colder temperatures and the electricity consumption increases with warmer temperatures. This suggests that the building has only natural gas for heating and electricity for cooling. 

![Scheme](proposal/multifuel fliptemp.png)

The table below shows a summary of all the different heating configurations I detected in the Marriott portfolio, based on the consumption data. I modeled all the Marriotts with either electric or chilled water cooling but found a few buildings with a positive correlation between natural gas and warmer temperatures suggesting they could have an engine driven chiller. If we had an external source to validate the presence of an engine driven chiller I could add that to the model in the future.


### Tuning Parameters
Each model I calibrated has five or six tuning parameters, based on the detected heating type. Every hotel has a parameter for cooling efficiency, lighting and plug load power, volume of domestic hot water, and the air changes per hour due to both ventilation and natural infiltration. Each model also has either electric heating efficiency, natural gas/steam heating efficiency, or both based on the heating detection. Additional parameters could be adding to the tuning process such as the efficiency of domestic hot water productions, if we had more information of on the type of systems and range of efficiencies present in hotels. There is really no limit to the number of tuning parameters that could be added to the model in the future. 

For buildings that use purchased chilled water for cooling I set the tuning range to be between (0.5 - 1.0 COP), and for all other buildings I used the range (1.5 - 5.2 COP) for electric cooling efficiency. For buildings that are heated with purchased steam I set the heating efficiency to vary between (50 - 100%). For buildings with only natural gas heat I used a heating efficiency range of (50 - 85%). For electric heating efficiency I used the range (0.5 - 3), to capture the range between electric baseboards and air-source heat pumps (ASHP). Buildings labeled as hybrid heated have the electric and natural gas/steam, heating efficiencies calibrated separately.

For lighting and plug load power I used a tuning range between (0 - 2 W/ft2). For domestic hot water I set the tuning range to be between zero and which ever is greater between the detected natural gas baseload or 0.025 gallons per square foot. For total ventilation I used a tuning range between 0.50 and 2.0 air changes per hour.

The specific values that are used for calibration are:
•	coolingCOP – total system cooling efficiency
•	electricHeatingEfficiency – total electric heating system efficiency (including heatpumps)
•	gasHeatingEfficiency – total gas heating efficiency
•	intPowerAndLit – total plugs and lights, in W/m2
•	vDotDHW - domestic hot water volumn rate, in m3/m2/year
•	ach – ventilation rate (outdoor air and infiltration) in ACH.

### Optimization Calibration
The industry standard method for calibrating energy models is for engineers to tune the model parameters by hand and re-run the the model many times until the predicted consumption from the model matches the actual data. I created a framework that uses optimization to automate the energy model calibration. This is done by using an algorithm to search for tuning parameters that optimally satisfy an objective function. The objective function I minimize is the error between the modeled energy use and the actual energy use and I measure that error with the magnitude of the CV-RMSE, coefficient of variation around the root mean square error. The CV-RMSE is a metric that ASHRAE specifies when creating a baseline model to be used in measurement and verification, and recommends only using models with a CV-RMSE less than 25% for predicting savings. 

The outcome of the process produces a calibrated energy model with the least error possible between the predicted consumption and the actual consumption. If the CV-RMSE is less than 25% then that model can be used to predict savings from retrofits. The outputs from the energy model include monthly and annual end-use breakdowns.

![Scheme](proposal/monthly calibration.png)

### Calibration Results
The calibration results below show that most Marriott hotels have efficient heating and cooling systems because the highest levels of efficiencies were selected. The hotels with an electric heating efficiency around 3 likely have air source heat pumps, and many buildings have a gas heating efficiency around 90% suggesting they have condensing boilers. Many hotels have a cooling COP around 3.5 - 6.5 suggesting they have ASHP in small hotels or water-cooled chillers in large hotels. The hotels have a wide range of ventilation rates, but still many have the minimum of 0.75 ACH. The distribution of natural gas baseload, consisting of domestic hot water consumption is centered around 25 gallons/ft2/year, but has a long tail with a few buildings using more than 50 gallons/ft2/year, these buildings could also be using natural gas for other services like laundry or cooking, which I did not explicitly model. 



### Predicting Savings
Once the model is calibrated and the predicted consumption from the model matches the actual utility bills we use this baseline model to estimate potential savings. I estimate the potential savings for each Marriott by comparing the calibrated model, or baseline to an efficient version of that building, and calculate the difference in energy consumption between the two models. Each month of consumption where the efficient model uses less energy than the baseline model is counted as savings. The efficient version of each Marriott starts with the baseline calibrated model parameters, but then has the highest bound for cooling COP (6.5), The highest bounds for electric and gas heating efficiency (90% and 3) respectively, and the lowest bound for ACH 0.75. For lighting and plug loads the efficient model has the minimum between the calibrated W/ft2 and 1.0 W/ft2 and for domestic hot water I do not estimate any savings and use the baseline parameter in the efficient model. We could add more specific rules to how we create the efficient models in the future. For example the large hotels could have a different efficient cooling COP than small hotels which represent the different types of cooling equipment in large vs small buildings. 


# Results 
The optimization calibration was successful for most of the hotels where a total of 282 or 96% overall passed the calibration check with a CV-RMSE < 25%. I noticed that buildings that start heating and cooling at extremely different temperatures and likely have simultaneous heating and cooling were the hardest to calibrate and have a high CV-RMSE. The overall portfolio of Marriotts have savings ranging from 0 - 50% energy savings with, most buildings in the range of 15 - 20% energy savings. 

### Example Score Card

In addition to analyzing results at the portfolio level, I also created a score card for each hotel to evaluate the heating detection, model calibration, overall and monthly end-use breakdown, selected tuning parameters, and monthly energy savings. 

![Scheme](proposal/Office_HighCooling_LowHeat.png)

