# set in config file
# This need to be tuned for each project.  This is the logic to assign the 
# bldgTypeName column which will then be used for model selection for 
# the normative model.

# Available building types 
# 'smallOffice','mediumOffice','largeOffice','smallHotel','largeHotel','quickServiceRestaurant'
consolidated_data$bldgTypeName <- "Hotel"
consolidated_data$bldgType <- "Hotel"
vintage <- "2004"

for(k in 1:nrow(consolidated_data)){
  if (consolidated_data$bldgTypeName[k] == "Hotel" & consolidated_data$squareFootage[k] < (1.0 * 10 ^ 5)) {
    consolidated_data$bldgType[k] <- "smallHotel"
  } else if (consolidated_data$bldgTypeName[k] == "Hotel" &
             consolidated_data$squareFootage[k] >= (1.0 * 10 ^ 5)
             & consolidated_data$squareFootage[k] < (3.5 * 10 ^ 5)) {
    consolidated_data$bldgType[k] <- "largeHotel"
  } else if (consolidated_data$bldgTypeName[k] == "Hotel" &
             consolidated_data$squareFootage[k] >= (3.5 * 10 ^ 5)) {
    consolidated_data$bldgType[k] <- "largeOffice"
  } else {
    consolidated_data$bldgType[k] <- "smallOffice"
  }
}