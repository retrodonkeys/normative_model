
Normative Model Development History

v2.0 rewrote methods for marriot input file
v2.1 parallelized optimization based calibration
v2.2 changed heating efficiency bounds = c(0.5, 1.0)
v2.3 dynamic model selection that includes small/large hotel and large office changes to visualization to include a graphic for energy savings\
v2.4 allowing overall heating efficiency to be optimized not just electric and changing ACH bounds to 0.75 - 2.0
v2.5 added changes to calibration bounds based on the presence of chilled water, steam, or all electric heating
v2.6 changed optimization method to SANN and lowered the HB to 0.90
v2.7 small graphic improvements, allowing cooling COP max at 6.5 for Water Cooled Chillers and also allowing for heating efficiency at 300% or 3 COP for ASHP if electic/hybrid heating using these improvements for for specific efficienct model specification. 
v3.0 rewrote optimization calibration into its own method. 