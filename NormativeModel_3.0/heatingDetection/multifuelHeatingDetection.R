
# Title: Multi-Fuel Heating Detection 
# Author: Brian Simmons 2017

# The is a functional wrapper of the getFlipTemp code to 
# detect heating type given electric and natural gas data

# The data should be normalized into the form Watt-hour/square foot/day
# The function returns the heating type and the normalized consumption per fuel type 

multifuelHeatingDetection = function (
  electricConsumptionNormalized,
  natrualGasConsumptionNormalized,
  tempF,
  climateZone) {
  
  df <- data.frame(
    electricConsumptionNormalized,
    natrualGasConsumptionNormalized,
    tempF
  )
  
  heatTypeThresh <- read.csv(
    "./heatingDetection/heatingTypeThresholds.csv", 
    as.is = T
  )
  
  
  # piece-wise regressions
  electricityFit <- getFlipTemp(
    temp = df$tempF,
    minFliptemp = 45,
    maxFliptemp = 65,
    consumption = df$electricConsumptionNormalized,
    plot = F
  )
  
  naturalGasFit <- getFlipTemp(
    temp = df$tempF,
    minFliptemp = 45,
    maxFliptemp = max(75,min(df$tempF)),
    consumption = df$natrualGasConsumptionNormalized,
    plot = T
  )
  
  
  # unit: kWh/m2/year
  maxVDotDHW <- (naturalGasFit$baseload * 365 / 1000) 

  elecHeatData <- subset(
    df,
    tempF <= electricityFit$fliptemp &
      electricConsumptionNormalized > electricityFit$baseload
  )
  
  gasHeatData <- subset(
    df,
    tempF <= naturalGasFit$fliptemp &
      natrualGasConsumptionNormalized > naturalGasFit$baseload
  )
  
  
  # estimated electricity heating, in Wh/ft2,day
  electricHeatNormalized <- sum(
    elecHeatData$electricConsumptionNormalized - electricityFit$baseload
  )
  
  # estimated natural gas heating, in Wh/ft2,day
  naturalGasHeatNormalized <- sum(
    gasHeatData$natrualGasConsumptionNormalized - naturalGasFit$baseload
  )
  
  
  # heating indicator threshold per climate zone 
  elecHeatThresh <- subset(heatTypeThresh,
                           Fuel.Type == "electricity" & Climate.Zone == climateZone,
                           select = Slope)
  

  gasHeatThresh <- subset(heatTypeThresh,
                          Fuel.Type == "natural gas" & Climate.Zone == climateZone,
                          select = Slope)
  
  
  # infer heating type
  heatingType <- NA
   
  #If slope was negative or line was linear than heatingIndictor fails.
  #This sets it to zero for the future logic loop.
  if(is.na(electricityFit$heatingIndicator)) electricityFit$heatingIndicator <- 0
  if(is.na(naturalGasFit$heatingIndicator)) naturalGasFit$heatingIndicator <- 0
    
  if (electricityFit$heatingIndicator < elecHeatThresh &
      naturalGasFit$heatingIndicator > gasHeatThresh) {
    naturalGasHeatNormalized <- 0
    heatingType <- "electric"
    
  } else if (electricityFit$heatingIndicator > elecHeatThresh &
             naturalGasFit$heatingIndicator < gasHeatThresh) {
    electricHeatNormalized <- 0 
    heatingType <- "natural gas"
    
  } else if (electricityFit$heatingIndicator < elecHeatThresh &
             naturalGasFit$heatingIndicator < gasHeatThresh) {
    electricHeatNormalized <- electricHeatNormalized
    naturalGasHeatNormalized <- naturalGasHeatNormalized
    heatingType <- "hybrid"
    
  } else {
    electricHeatNormalized <- 0
    naturalGasHeatNormalized <- 0
    heatingType <- "not heated"
    
  }
  
  simultaneousHCYN <- if(heatingType == "electric" | heatingType == "not heated"){
      FALSE
    } else if(abs(electricityFit$fliptemp - naturalGasFit$fliptemp) < 
              electricityFit$fliptempSE + naturalGasFit$fliptempSE) {
        "Possible"
      } else if (electricityFit$fliptemp < naturalGasFit$fliptemp) {
        TRUE
      } else {
        FALSE
      }
  
  
  
  return(list(
    heatingType = heatingType,
    simultaneousHCYN = simultaneousHCYN,
    electricHeatNormalized = electricHeatNormalized,
    naturalGasHeatNormalized = naturalGasHeatNormalized,
    electricFit = electricityFit$out.seg,
    electricFlipTemp = electricityFit$fliptemp,
    electricFlipTempSE = electricityFit$fliptempSE,
    naturalGasFit = naturalGasFit$out.seg,
    naturalGasFlipTemp = naturalGasFit$fliptemp,
    naturalGasFlipTempSE = naturalGasFit$fliptempSE,
    maxVDotDHW = maxVDotDHW
  ))

}
