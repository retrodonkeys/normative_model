# slightly modified getFlipTemp function (VEA package v ???)
getNatGasFlipTemp <- function(
  temp, consumption, 
  minFliptemp=35,
  maxFliptemp=65,
  heatIndThrd = -0.001,
  coolIndThrd = 0.05,
  tempBinSize = 2,
  plot=TRUE, 
  xlab=NULL, 
  ylab=NULL, 
  main=NULL, 
  pointCol="black", 
  lineCol="blue", 
  xlim=NULL, 
  ylim=c(min(consumption,na.rm=T)*0.9, max(consumption,na.rm=T)),
  show.stats = FALSE,
  savePNG=FALSE, 
  savePNGDir=getwd(),
  pngFileName=paste("Fliptemp -",main))

{
  
  # remove NAs
  if(is.na(min(temp,consumption))) {
    complete <- complete.cases(temp,consumption)
    temp <- temp[complete]
    consumption <- consumption[complete]
  }
  
  consumption3 <- round(as.numeric(consumption), 3)
  
  if (max(consumption3) == min(consumption3)) {
    # if consumption is a flat line
    
    out <- list(fliptemp = NA, 
                fliptempSE = NA,
                rmse = NA,
                cvrmse = NA,
                heatingIndicator = 0, 
                heatingIndicatorSE = 0,
                heatingType = "N-NG",
                heatingTypeConfidence = 1,
                baseload = mean(consumption, 
                                na.rm = TRUE),
                predicted = consumption,
                out.seg = cbind(c(min(temp), mean(temp), max(temp)), 
                                rep(mean(consumption), 3)))
  } else {
    
    # if consumption is not a flat line
    suppressMessages(require(zoo))
    suppressMessages(require(hydroGOF))
    
    input <- data.frame(temp, consumption)
    
    ## Bin generation
    sortedTemp <- sort(unique(temp), decreasing = FALSE)
    thirdHighTemp <- sortedTemp[length(sortedTemp) - 2]
    thirdLowTemp <- sortedTemp[3]
    
    if(thirdLowTemp > maxFliptemp) {
      # if third lowest temp is > 65 deg F, no heating
      
      # average consumption is baseload
      baseload <- mean(consumption, na.rm = TRUE)
      
      ## Output results
      out <- list(fliptemp = NA,
                  fliptempSE = NA,
                  rmse = NA,
                  cvrmse = NA,
                  heatingIndicator = 0, 
                  heatingIndicatorSE = 0,
                  heatingType = "N-NG",
                  heatingTypeConfidence = 1,
                  baseload = baseload,
                  predicted = consumption,
                  out.seg = cbind(c(min(temp), mean(temp), max(temp)), 
                                  rep(mean(consumption, na.rm = TRUE), 3)))
      
    } else if(thirdHighTemp < maxFliptemp) {
      
      # if third highest temp is < 35 deg F, fliptemp = maxtemp and fit all
      # points to detect heating type
      all.lm <- lm(consumption ~ temp)
      # save slope as heating indicator
      heatingIndicator <- min(as.numeric(all.lm$coefficients[2]),0)
      # prediction
      predicted <- sort(all.lm$fitted.values)
      # rmse
      rmse <- rmse(consumption, predicted)
      # calculate standard error
      heatingIndicatorSE <- sqrt(sum((consumption - predicted)^2)/
                                   (length(temp)-2)) /
        sqrt(sum((temp - mean(temp))^2))
      # save max temp as flip temp and predicted consumption at max temp as baseload
      flipTemp <- sortedTemp[length(sortedTemp)]
      
      # save min consumption as baseload
      baseload <- as.numeric(predicted[1])
      
      ## Output results
      out <- list(fliptemp = flipTemp,
                  fliptempSE = ((max(temp, na.rm = TRUE) -
                                   min(temp, na.rm = TRUE)) / 
                                  (max(consumption, na.rm = TRUE) - 
                                     min(consumption, na.rm = TRUE))) * rmse,
                  rmse = rmse,
                  cvrmse = rmse / mean(consumption, na.rm = TRUE),
                  heatingIndicator = heatingIndicator, 
                  heatingIndicatorSE = heatingIndicatorSE,
                  heatingType = if(heatingIndicator <= heatIndThrd) {
                    "NG"} else {"N-NG"},
                  heatingTypeConfidence = max(pnorm(heatIndThrd,
                                                    mean = heatingIndicator,
                                                    sd = heatingIndicatorSE),
                                              1 - pnorm(heatIndThrd,
                                                        mean = heatingIndicator,
                                                        sd = heatingIndicatorSE)),
                  baseload = baseload,
                  predicted = predicted,
                  out.seg = cbind(c(min(temp, na.rm = TRUE), 
                                    flipTemp, flipTemp), 
                                  c(max(predicted, na.rm = TRUE), 
                                    baseload, baseload)))
      
    } else {
      # if third highest temp is not < 65 deg F
      tempBinSize <- 2
      FTFrom <- max(thirdLowTemp,minFliptemp)
      FTTo <- min(thirdHighTemp,maxFliptemp)
      FT <- seq(from = FTFrom, to = FTTo, by = tempBinSize)
      nbGrid <- length(FT)
      
      YFT <- rep(NA, nbGrid)
      leftSlope <- rep(NA, nbGrid)
      # rightSlope <- rep(NA, nbGrid)
      RMSE <- rep(NA,nbGrid)
      pred <- matrix(rep(NA,length(temp)*nbGrid),length(temp),nbGrid)
      
      leftIntercept <- rightIntercept <- rep(NA, nbGrid)
      
      for (i in 1:nbGrid)
      {
        leftConsumption <- input[temp<=FT[i],2]
        leftTemp <- input[temp<=FT[i],1]
        left.lm <- lm(leftConsumption ~ leftTemp)
        leftIntercept[i] <- as.numeric(left.lm$coefficients[1])
        leftSlope[i] <- as.numeric(left.lm$coefficients[2])
        
        if(leftSlope[i] > 0) {
          leftSlope[i] <- 0
          leftIntercept[i] <- mean(leftConsumption)
        }
        
        YFT[i] <- FT[i] * leftSlope[i] + leftIntercept[i]
        
        rightConsumption <- input[temp>=round(FT[i],3), 2] - YFT[i]
        rightTemp <- input[temp >= round(FT[i],3),1] - FT[i]
        
        # right.lm <- lm(I(rightConsumption) ~ I(rightTemp) + 0)
        # 
        # rightSlope[i] <- as.numeric(right.lm$coefficients)
        # rightIntercept[i] <- mean(rightConsumption)
        
        # # Adjust slopes
        # rightSlope[i] <- max(0, rightSlope[i])
        
        # predicted
        for (j in 1:length(temp))
        {
          if (temp[j] <= FT[i])
          {
            pred[j,i] <- leftSlope[i] * (temp[j] - FT[i]) + YFT[i]
            
          } else {
            pred[j,i] <- YFT[i]
          }
        }
        
        # RMSE
        RMSE[i] <- rmse(consumption, as.numeric(pred[,i]))
      } # end of loop
      
      ## output
      besti <- which(RMSE==min(RMSE, na.rm=T))[1]
      if(length(besti)>1) besti <- besti[which(abs(temp[besti]-50)==min(abs(temp[besti]-50)))]
      
      fliptemp <- FT[besti]
      predicted <- pred[,besti]
      heatingIndicator = leftSlope[besti]
      # coolingIndicator = rightSlope[besti]
      
      minTempPred <- leftSlope[besti] * (min(temp) - fliptemp) + YFT[besti]
      maxTempPred <- YFT[besti]
      
      # standard errors
      fliptempSE = ((max(temp, na.rm=T)-min(temp, na.rm=T)) / 
                      (max(consumption, na.rm=T)-min(consumption, na.rm=T))) * RMSE[besti]
      
      hIndex <- temp<=fliptemp
      if(sum(hIndex)<3)
      {
        heatingIndicatorSE <- NA
      } else {
        heatingIndicatorSE = sqrt(sum((consumption[hIndex]-predicted[hIndex])^2)/(length(temp[hIndex])-2)) /
          sqrt(sum((temp[hIndex]-mean(temp[hIndex]))^2))
        # http://stattrek.com/regression/slope-confidence-interval.aspx
      }
      
      ## Output results
      out <- list(fliptemp = fliptemp, 
                  fliptempSE = ((max(temp, na.rm=T)-min(temp, na.rm=T)) / 
                                  (max(consumption, na.rm=T)-min(consumption, na.rm=T))) * RMSE[besti],
                  rmse=RMSE[besti],
                  cvrmse=RMSE[besti]/mean(consumption, na.rm=T),
                  heatingIndicator = heatingIndicator, 
                  heatingIndicatorSE = heatingIndicatorSE,
                  heatingType = if(heatingIndicator<=heatIndThrd) "NG" else "N-NG",
                  heatingTypeConfidence = max(pnorm(heatIndThrd,mean=heatingIndicator,sd=heatingIndicatorSE),
                                              1-pnorm(heatIndThrd,mean=heatingIndicator,sd=heatingIndicatorSE)),
                  baseload = YFT[besti],
                  predicted = predicted,
                  out.seg = cbind(c(min(temp), fliptemp, max(temp)), 
                                  c(minTempPred, YFT[besti], maxTempPred)))
    }
    
  } # end of calc
  
  if (plot)
  {
    # save files
    if(savePNG) 
    {
      png(filename = paste0(savePNGDir, '/',pngFileName, ".png"), width = 1100, height = 850)
    }
    
    maxCons <- max(consumption, na.rm = TRUE)
    minCons <- min(consumption, na.rm = TRUE)
    
    plot(temp, consumption, 
         xlab = xlab, ylab = ylab, 
         main = main, 
         ylim = ylim, xlim = xlim, 
         col = pointCol)
    
    if(show.stats) {
      mtext(paste0("Fliptemp:", round(fliptemp, 1), " F, ",
                   "HI:", round(heatingIndicator, 3), ", ",
                   "Heat:", out$heatingType, " (", 
                   round(out$heatingTypeConfidence * 100),"%), ",
                   "CI:", round(coolingIndicator, 3), ", ",
                   "Cool:", out$coolingType, " (",
                   round(out$coolingTypeConfidence * 100), "%)"))
    }
    
    # plot fit
    lines(out$out.seg, col = lineCol) 
    
    points(x = out$fliptemp, y = minCons * 0.9, col = lineCol, pch = 19)
    segments(x0 = out$fliptemp - out$fliptempSE, y0 = minCons * 0.9, 
             x1 = out$fliptemp + out$fliptempSE, y1 = minCons * 0.9, col = lineCol)
    
    if(savePNG) dev.off()
  }
  
  return(out)
  
}